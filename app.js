require('dotenv').config();

const TelegramBot = require('node-telegram-bot-api');
const fs = require('fs');

const token = process.env.TELEGRAM_TOKEN;

const bot = new TelegramBot(token, { polling: true });

var stats;

checkFileExistence = path => {
    try {
        if (fs.existsSync(path)) {
            // stats file exsists
            return true;
        }
    } catch (err) {
        return false;
    }
};

updateFile = (path, source) => {
    fs.writeFile(path, JSON.stringify(source), function(err) {
        if (err) throw err;
    });
};

bot.onText(/\/pidorreg/i, (msg, match) => {
    const chatId = msg.chat.id;
    const user = msg.from;
    const statsPath = 'db/' + chatId + '.json';
    if (checkFileExistence(statsPath)) {
        stats = JSON.parse(fs.readFileSync(statsPath));
    } else {
        fs.appendFile(statsPath, '', function(err) {
            if (err) throw err;
        });
        stats = {
            players: {},
            lastGame: {
                date: 0
            }
        };
    }

    if (stats.players[user.id] != undefined) {
        bot.sendMessage(chatId, 'Игрок существует');
    } else {
        if (user.username != undefined) {
            stats.players[user.id] = { name: '@' + user.username };
        } else {
            stats.players[user.id] = { name: user.first_name };
        }
        stats.players[user.id].score = 0;

        stats.playersKeys = Object.keys(stats.players);

        updateFile(statsPath, stats);
        bot.sendMessage(chatId, 'Игрок зарегистрирован');
    }
});

bot.onText(/\/pidor+(@|$)/i, (msg, match) => {
    const chatId = msg.chat.id;
    const date = new Date() / 1000;
    const statsPath = 'db/' + chatId + '.json';

    if (checkFileExistence(statsPath)) {
        stats = JSON.parse(fs.readFileSync(statsPath));
    } else {
        bot.sendMessage(
            chatId,
            'В чате нет игроков, /pidorreg для регистрации'
        );
        return;
    }

    // if (date - stats.lastGame.date < 86400){
    //   bot.sendMessage(chatId, 'Победитель последней игры - ' + stats.lastGame.winner);
    //   return
    // }

    winnerKey =
        stats.playersKeys[(stats.playersKeys.length * Math.random()) << 0];

    stats.players[winnerKey].score++;

    bot.sendMessage(chatId, 'Победитель - ' + stats.players[winnerKey].name);

    stats.victories = [];
    for (var player in stats.players) {
        stats.victories.push([
            stats.players[player].name,
            stats.players[player].score
        ]);
    }

    stats.victories.sort(function(a, b) {
        return b[1] - a[1];
    });

    stats.lastGame.winner = stats.players[winnerKey].name;
    stats.lastGame.date = msg.date;
    updateFile(statsPath, stats);
});

bot.onText(/\/pidorstats/i, (msg, match) => {
    const chatId = msg.chat.id;
    const statsPath = 'db/' + chatId + '.json';

    if (checkFileExistence(statsPath)) {
        stats = JSON.parse(fs.readFileSync(statsPath));
    } else {
        bot.sendMessage(chatId, 'В чате не проходили игры, /pidor для запуска');
        return;
    }

    list = 'Топ игроков:\n';
    for (i = 0; i < stats.victories.length; i++) {
        list +=
            i +
            1 +
            '. ' +
            stats.victories[i][0] +
            ' - ' +
            stats.victories[i][1] +
            '\n';
    }

    bot.sendMessage(chatId, list);
});
